# -----------------------------------------------------------------------------
# DTM Stats
# -----------------------------------------------------------------------------

# Prep ASR -- Won't need this if you ran 2_rancors.R
dtm_asr <- dtm_stopper(dtm_asr, stop_docprop = c(.4, Inf)) #get rid of words that
#appear in less than 40% of docs
dtm_asr <- dtm_asr[Matrix::rowSums(dtm_asr) != 0,]


# Prep CR -- Won't need this if you ran 2_rancors.R
dtm_con <- dtm_stopper(dtm_con, stop_docprop = c(.01, Inf)) #get rid of words that
#appear in less than 1% of docs
dtm_con <- dtm_con[Matrix::rowSums(dtm_con) != 0,]

# Get the stats
dca_stats <- dtm_stats(dtm_dca)
dca_st_stats <- dtm_stats(dtm_dca_st)
asr_stats <- dtm_stats(dtm_asr)
con_stats <- dtm_stats(dtm_con)

# Single table
stats <- data.frame(matrix(ncol = 4, nrow = 4))
colnames(stats) <- c("DoCA", "DoCA_no_st", "CR", "ASR")
rownames(stats) <- c("N_Docs", "N_Tokens", "N_Vocab", "Mean_Tokens")

stats[1,1] <- dca_stats[[1]][2]$Value[1]
stats[2,1] <- dca_stats[[1]][2]$Value[4]
stats[3,1] <- dca_stats[[1]][2]$Value[3]
stats[4,1] <- dca_stats[[4]][2]$Value[2]

stats[1,2] <- dca_st_stats[[1]][2]$Value[1]
stats[2,2] <- dca_st_stats[[1]][2]$Value[4]
stats[3,2] <- dca_st_stats[[1]][2]$Value[3]
stats[4,2] <- dca_st_stats[[4]][2]$Value[2]

stats[1,3] <- con_stats[[1]][2]$Value[1]
stats[2,3] <- con_stats[[1]][2]$Value[4]
stats[3,3] <- con_stats[[1]][2]$Value[3]
stats[4,3] <- con_stats[[4]][2]$Value[2]

stats[1,4] <- asr_stats[[1]][2]$Value[1]
stats[2,4] <- asr_stats[[1]][2]$Value[4]
stats[3,4] <- asr_stats[[1]][2]$Value[3]
stats[4,4] <- asr_stats[[4]][2]$Value[2]

print(stats)


