# -----------------------------------------------------------------------------
# Get CMD for Sensorimotor N-Directions
# -----------------------------------------------------------------------------

# function to perform the pool offset n-direction method
get_ndirections <- function(anchors, wv){
  
  .nvec_offset <- function(i){
    as.matrix(cvec[i, , drop = FALSE] - 
                colMeans(cvec[-i, ]))
  }
  
  out <- lapply(anchors, get_centroid, ft.wv)
  cvec <- do.call(rbind, out)
  
  out <- lapply(seq_len(nrow(cvec)), .nvec_offset)
  return(do.call(rbind, out))
  
}

# -----------------------------------------------------------------------------
#
# -----------------------------------------------------------------------------

aud_anchors <- c("auditory", "hear", "ear", "audible")
gus_anchors <- c("gustatory", "taste", "tongue", "mouth")
hap_anchors <- c("haptic", "touch", "hands", "tactile", "grip", "touching")
olf_anchors <- c("olfactory", "smell", "nose", "aroma")
vis_anchors <- c("optic", "color", "colorful","show", "see", "vision")

anchor_list <- list(aud_anchors, 
                    gus_anchors, 
                    hap_anchors,
                    olf_anchors,
                    vis_anchors)

n_dirs <- get_ndirections(anchors=anchor_list, wv=ft.wv)

# -----------------------------------------------------------------------------
# Validity Table
# -----------------------------------------------------------------------------

source("scripts/3b_validity_tests.R")

cat("\nValidity (Table 2):\n \n")
print(df_validity)

cmd_out <- text2map::CMDist(
  dtm=dtm_dca,
  cv = n_dirs,
  wv = ft.wv, 
  scale = TRUE
)

# -----------------------------------------------------------------------------
# Append CMD scores to metadata
# -----------------------------------------------------------------------------

colnames(cmd_out) <- c("doc_id", "auditory_cmd", "gustatory_cmd",
                       "haptic_cmd", "olfactory_cmd", "visual_cmd")

df_out_doca <- left_join(df_out_doca, cmd_out, by =c("pdf_file" = "doc_id") )

# -----------------------------------------------------------------------------
# OOV Table
# -----------------------------------------------------------------------------

source("scripts/3c_oov.R")
cat("\nOOV (Table 3):\n \n")
print(df_oov)

# -----------------------------------------------------------------------------
# Highest CMD Score per Modality Table
# -----------------------------------------------------------------------------

source("scripts/3d_cmd_ranks.R")
cat("\nOOV (Table 4):\n \n")
print(ranks)

# ------------------------------------------------------------------------------
# THE END
# ------------------------------------------------------------------------------
