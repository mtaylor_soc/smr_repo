# Replication repository for "A Tool Kit for Relation Induction in Text Analysis"

[Dustin S. Stoltz](https://www.dustinstoltz.com/), *Lehigh University*  
[Marshall A. Taylor](https://marshalltaylor.net/), *New Mexico State University*  
[Jennifer S.K. Dudley](https://www.jenniferdudley.info/), *Columbia Business School*  

This repository contains all `R` code and data necessary to replicate the paper, currently under review at *Sociological Methods and Research*.

In the paper, we discuss the limitations of using hand-weighted dictionaries as gradational measures at the document- and author-level. Distances derived from word embeddings can measure a range of gradational relations---semantic similarity, hierarchy, entailment, and stereotype---and can be used at the document- and author-level in ways that overcome these limitations. We provide a comprehensive introduction to using word embeddings for relation induction, and demonstrate how such techniques can complement dictionary methods as unsupervised, deductive methods.

Please open 0_start_here.R to get started.
